#!/bin/sh
perl -pi.bak -e 's/var VERSION="v(\d+\.\d+)\.(\d+)"/"var VERSION=\"v".$1.".".($2+1)."\""/e' source/global.js && tools/deploy.sh -B true && perl -pi.bak -e 's/"version": *"(\d+\.\d+)\.(\d+)"/"\"version\": \"".$1.".".($2+1)."\""/e' manifest.webapp && cp manifest.webapp deploy
grep "version" "deploy/manifest.webapp"