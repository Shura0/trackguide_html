
var APP;
var MAIN;
enyo.kind({
	name: "App",
  kind: "FittableRows",
  classes: "enyo-unselectable enyo-fit",
  components: [
    {name: "popup", kind: 'onyx.Popup', centered: true, floating: true, modal:true, classes:"onyx-sample-popup", style: "padding: 10px;", content: "Loading..."},
  ],
  create: function() {
    this.inherited(arguments);
  },
  rendered: function(){
    this.inherited(arguments);
    this.$.popup.show();
    this.done();
  },
  done: function(firstrun) {
    this.$.popup.hide();
    if(!MAIN)MAIN=new MainScene();
    MAIN.renderInto(document.body);
  },
  showMain: function() {
    if(!MAIN)MAIN=new MainScene();
    MAIN.renderInto(document.body);
  }
});


enyo.kind({
	name: "MainScene",
  kind: 'Panels',
  classes:"enyo-panels",
  arrangerKind: "LeftRightArranger",
  margin:0,
  index:0,
  draggable:false,
  wrap:false,
  fit:true,
	components: [
    {kind: 'FilesScene', name:"files"},
    {kind: 'GuideScene', name:"guide"},
    //{kind: 'CompassScene', name: "compass"},
  ],
	rendered: function() {
		this.inherited(arguments);
    this.start();
  },
  start:function() {
    //console.log("Start. targets="+TARGETS.length);
    this.setIndex(0);
    this.$.files.setBack(enyo.bind(this,"toGuide"));
    this.$.guide.setBack(enyo.bind(this,"toFiles"));
    this.$.files.start.call(this.$.files);
  },
  create: function() {
    this.inherited(arguments);
    this.screenLock=0;
  },
  toFiles:function() {
    this.setIndex(0);
    this.$.files.start.call(this.$.files);
  },
  toGuide:function() {
    this.setIndex(1);
    this.$.guide.start();
  },
  popup: function(message)
  {
    this.$.popup.setContent(message);
    this.$.popup.show();
  },
  reflow: function() {
		this.inherited(arguments);
	},

  closePopup: function(){
    this.$.deleteConfirmation.hide();
  },
  closeAllPopup: function(){
    this.$.deleteAllConfirmation.hide();
  },
});
//APP=new App();
//APP.done(0);
loadSettings();
console.log("VERSION: "+VERSION);
MAIN=new MainScene();
MAIN.renderInto(document.body);

