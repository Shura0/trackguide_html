enyo.kind({
  name: 'GuideScene',
  kind: 'FittableRows',
  classes: "enyo-fit bk",
  components: [
    {kind: "onyx.Toolbar", components: [
        {components: [
            {kind: "FittableColumns", classes: "enyo-fit", style: "padding:6px", components: [
                {kind: "onyx.IconButton", src: "assets/images/common-back.png", ontap: "tback", style: "height:32px"},
                {fit: true, content: "Turns: 0 of 0", style: "text-align:center", name: "header"},
                {kind: "onyx.MenuDecorator", onSelect: "menuSelected", components: [
                    {kind: "onyx.IconButton", src: "assets/images/menu.png"},
                    {kind: "onyx.Menu", floating: true, classes: "menu", components: [
                        //{content: "To the closest point"},
                        //{content: "Start again"},
                        //{classes: "onyx-menu-divider"},

                        //{content: "Sound on turn", name: "soundMenu"},
                        {name: "soundMenu", components: [
                            {kind: "onyx.IconButton", src: "assets/images/checkbox_unchecked.png", name: "soundMenuIcon"},
                            {content: "Sound on turn"}
                          ]},
                        {name: "trackMenu", components: [
                            {kind: "onyx.IconButton", src: "assets/images/checkbox_unchecked.png", name: "trackMenuIcon"},
                            {content: "Display track & map"}
                          ]},
                        {content: "Dark theme", name: "themeMenu"},
                        //{content: "Display track & map", name: "trackmenu"},

                        {content: "Start from here", name: "startMenu"},
                        {classes: "onyx-menu-divider"},
                        {content: "About", name: "aboutMenu"}
                      ]}
                  ]}
              ]}
          ]}
      ]},
    {name: "popup", kind: 'onyx.Popup', centered: true, floating: true, modal: true,
      allowHtml: true, style: "padding: 10px;"},
    {classes: "center", name: "turnHolder", components: [
        {name: "turn", kind: "Image", classes: "turn"}
      ]},
    {classes: "center", name: "compassHolder", components: [
        {name: "compass", kind: "Image", classes: "compass"}
      ]},
    {classes: "center", name: "distanceHolder", components: [
        {name: "distance"}
      ]},
    {kind: "enyo.Audio", name: "turnSound", src: "assets/audio/turn.ogg"},
    {classes: "map", name: "mapHolder", components: [
        {kind: "enyo.Canvas", name: "map", attributes: {width: 320, height: 200}, components: [
          ]
        }
      ]}
  ],
  create: function() {
    this.inherited(arguments);
    this.$.turn.setSrc('assets/images/nogps.png');
    this.$.compass.setSrc('assets/images/arrows/00.png');
  },
  rendered: function() {
    this.inherited(arguments);
    //this.startGeowatching();
    console.log("guide rendered");
  },
  start: function() {
    if ((!this.screenLock) && window.navigator.requestWakeLock)
      this.screenLock = window.navigator.requestWakeLock('screen');

    CURPOINT = 0;
    this.$.distance.setContent("Unknown");
    var v = this.$.mapHolder.getBounds();
    console.log(v);
    this.$.map.setAttribute("width", v.width - v.left);
    this.$.map.setAttribute("height", v.height - v.top);
    this.startGeowatching();
    if (SETTINGS.dark)
    {
      this.removeClass("bk");
      this.addClass("bk_dark");
      this.$.distanceHolder.removeClass("center");
      this.$.distanceHolder.addClass("center_dark");
      this.$.turnHolder.removeClass("center");
      this.$.turnHolder.addClass("center_dark");
      this.$.compassHolder.removeClass("center");
      this.$.compassHolder.addClass("center_dark");
      this.$.mapHolder.removeClass("center");
      this.$.mapHolder.addClass("center_dark");
      this.$.themeMenu.setContent("Bright theme");
    }
    else
    {
      this.addClass("bk");
      this.removeClass("bk_dark");
      this.$.distanceHolder.removeClass("center_dark");
      this.$.distanceHolder.addClass("center");
      this.$.turnHolder.removeClass("center_dark");
      this.$.turnHolder.addClass("center");
      this.$.compassHolder.removeClass("center_dark");
      this.$.compassHolder.addClass("center");
      this.$.mapHolder.removeClass("center_dark");
      this.$.mapHolder.addClass("center");
      this.$.themeMenu.setContent("Dark theme");
    }
    if (SETTINGS.turnsound)
    {
      this.$.soundMenuIcon.setSrc("assets/images/checkbox_checked.png");
    }
    else
    {
      this.$.soundMenuIcon.setSrc("assets/images/checkbox_unchecked.png");
    }
    this.$.map.destroyComponents();
    if (SETTINGS.displaytrack)
    {
      this.$.trackMenuIcon.setSrc("assets/images/checkbox_checked.png");
      this.$.mapHolder.applyStyle("border", "solid 1px;");

      this.$.map.createComponent(
              {kind: "enyo.my.prepare", outlineColor: "", color: ""});
      if (waters)
      {
        for (var i = 0; i < waters.length; i++)
        {
          this.$.map.createComponent(
                  {kind: "enyo.my.water", water: waters[i], color: "", outlineColor: ""},
          {addBefore: this.$.mapTrack});
        }
      }
      if (buildings)
      {
        for (i = 0; i < buildings.length; i++)
        {
          this.$.map.createComponent(
                  {kind: "enyo.my.building", building: buildings[i], color: "", outlineColor: ""},
          {addBefore: this.$.mapTrack});
        }
      }
      if (highways)
      {
        for (i = 0; i < highways.length; i++)
        {
          var h = highways[i].properties.highway;
          if (h !== "primary" && h !== "secondary" && h !== "secondary_link" &&
                  h !== "motorway" && h !== "motorway_link" && h !== "trunk" &&
                  h !== "trunk_link")
            this.$.map.createComponent(
                    {kind: "enyo.my.highway", highway: highways[i], color: "", outlineColor: ""},
            {addBefore: this.$.mapTrack});
        }
      }
      if (highways)
      {
        for (i = 0; i < highways.length; i++)
        {
          h = highways[i].properties.highway;
          if (h === "primary" || h === "secondary" || h === "secondary_link" ||
                  h === "motorway" || h === "motorway_link" || h === "trunk" ||
                  h === "trunk_link")
            this.$.map.createComponent(
                    {kind: "enyo.my.highway_primary", highway: highways[i], color: "", outlineColor: ""},
            {addBefore: this.$.mapTrack});
        }
      }
      this.$.map.createComponent(
              {kind: "enyo.my.track", name: "mapTrack", outlineColor: "", color: ""});
      this.$.map.createComponent({kind: "enyo.my.finish", name: "finishDraw"});
      this.$.map.createComponent(
              {kind: "enyo.my.arrow", name: "mapArrow", color: "green"});
    }
    else
    {
      this.$.trackMenuIcon.setSrc("assets/images/checkbox_unchecked.png");
      this.$.mapHolder.applyStyle("border", "none");
    }

    this.$.map.update();
    if (POINTS[0] && NUMPOINTS)
    {
      this.$.header.setContent("Turn " + (POINTS[0].id + 1) + " of " + NUMPOINTS);
    }

  },
  startGeowatching: function() {
    if (this.job)
      return;
    this.job = navigator.geolocation.watchPosition(enyo.bind(this, "updatePosition"), enyo.bind(this, "getPositionFailed"), {timeout: 30000});
    this.counter = 0;
    console.log("geowatching started");
  },
  stopGeowatching: function() {
    navigator.geolocation.clearWatch(this.job);
    this.job = 0;
    console.log("geowatching stopped");
  },
  updatePosition: function(pos) {
    //console.log("Update position");
    LOCATION.lon = pos.coords.longitude;
    LOCATION.lat = pos.coords.latitude;
    var angle;
    if (isNaN(pos.coords.heading))
    {
      angle = 0;
      DIRECTION.lat = 0;
      DIRECTION.lon = 0;
    }
    else
    {
      angle = pos.coords.heading / 180 * Math.PI;
    }
    this.counter++;
    if (angle !== this.prevangle)
    {
      var d = new Point(Math.cos(angle) * 0.85, Math.sin(angle) * 0.85);
      if (d.lat || d.lon)
      {
        DIRECTION = calcDirection(DIRECTION, d);
        ANGLE_DIRECTION = Angle(DIRECTION, {lon: 0, lat: 1});
      }
      this.prevangle = angle;
    }
    if (POINTS[0])
    {
      this.prevdistance = POINTS[0].distance;
      POINTS[0].calcDistance(LOCATION);
      if (POINTS[0].distance < 100)
      {
        //turn on screen if it is off
      }
      POINTS[0].calcDirection(LOCATION, DIRECTION);
      var dir = POINTS[0].dir;
      if (dir >= 7 && dir <= 9) //wrong way. Moving straight to the next point
      {
        WRONGDIRECTION++;
      }
      else
      {
        if (WRONGDIRECTION > 0)
          WRONGDIRECTION--;
      }
      if ((POINTS[0].distance < 35/*meters*/ && POINTS[0].distance > this.prevdistance) ||
              (POINTS[0].distance < 20))
      {
        CURPOINT = POINTS[0].oid;
        POINTS.splice(0, 1); //go to the next point
        WRONGDIRECTION = 0;
        if (SETTINGS.turnsound)
          this.$.turnSound.play();
        if (!POINTS[0])
        {
          this.$.compass.setSrc('assets/images/arrows/00.png');
          this.$.distance.setContent("Finished");
          return;
        }
        this.$.header.setContent("Turn " + (POINTS[0].id + 1) + " of " + NUMPOINTS);
        return;
      }
      if (POINTS[1])
      {
        POINTS[1].calcDirection(POINTS[0].point, DIRECTION);
        this.$.turn.setSrc(POINTS[1].image);
      }
      else
      {
        this.$.turn.setSrc('assets/images/turns/finish.png');
      }
      this.$.compass.setSrc(POINTS[0].image2);
      this.$.distance.setContent(POINTS[0].distance + " m");

      if (this.counter > 4) //once in 5 updates
      {
        var mind = POINTS[0].distance;//meters
        var id = -1;
        this.counter = 0;
        for (var i = 1; i < 10; i++) //we can cut off not more than 10 points
        {
          if (!POINTS[i])
            break;
          POINTS[i].calcDirection(LOCATION, DIRECTION);
          POINTS[i].calcDistance(LOCATION);
          if (POINTS[i].distance < 20)
          {
            id = i;
            break;
          }
          if (Math.abs(POINTS[i].direction) < 1.04) //straight on our way
          {
            //POINTS[i].calcDistance(LOCATION);
            if (POINTS[i].distance < mind && WRONGDIRECTION > 20)
            {
              mind = POINTS[i].distance;
              id = i;
            }
          }
        }
        if (id > 0)
        {
          console.log("Cut off point!");
          CURPOINT = POINTS[id - 1].oid;
          POINTS.splice(0, id);
          WRONGDIRECTION = 0;
          if (SETTINGS.turnsound)
            this.$.turnSound.play();
          if (!POINTS[1])
            this.$.turn.setSrc('assets/images/turns/finish.png');
          if (!POINTS[0])
          {
            this.$.compass.setSrc('assets/images/arrows/00.png');
            this.$.distance.setContent("Finished");
            return;
          }
          this.$.header.setContent("Turn " + (POINTS[0].id + 1) + " of " + NUMPOINTS);
        }
      }
    }
    if (SETTINGS.displaymap || SETTINGS.displaytrack)
    {
      this.$.map.update();
    }
  },
  getPositionFailed: function(e) {
    console.log("Get position failed");
    var message = 0;
    this.$.compass.setSrc('assets/images/arrows/00.png');
    this.$.turn.setSrc('assets/images/nogps.png');
    switch (e.code)
    {
      case 1:
        message = "Permission denied\nPlease allow the program to use geolocation";
        break;
      case 2:
        message = "Impossible to get your geolocation. Is GPS enabled?";
        break;
      case 3:
        message = "GPS timeout";
        break;
      default:
        message = "Unknown GPS error: " + e.code;
    }
    if (message)
      console.log(message);
  },
  menuSelected: function(inSender, inEvent) {
    //Menu items send an onSelect event with a reference to themselves & any directly displayed content
    console.log(this.name);
    console.log(inEvent.originator);
    console.log(inEvent.originator.content);
    if (inEvent.originator)
    {
      switch (inEvent.originator.name)
      {
        case "soundMenu":
          SETTINGS.turnsound = !SETTINGS.turnsound;
          saveSettings();
          if (SETTINGS.turnsound)
          {
            this.$.soundMenuIcon.setSrc("assets/images/checkbox_checked.png");
          }
          else
          {
            this.$.soundMenuIcon.setSrc("assets/images/checkbox_unchecked.png");
          }
          break;
        case "themeMenu":
          SETTINGS.dark = !SETTINGS.dark;
          saveSettings();
          this.start();
          break;
        case "trackMenu":
          SETTINGS.displaytrack = !SETTINGS.displaytrack;
          saveSettings();
          this.start();
          break;
        case "startMenu":
          if ((!POINTS) || (!POINTS[0]))
            break;
          var mind = POINTS[0].distance; //meters
          var id = -1;
          for (var i = 1; i < POINTS.length; i++)
          {
            if (!POINTS[i])
              break;
            POINTS[i].calcDirection(LOCATION, DIRECTION);
            POINTS[i].calcDistance(LOCATION);
            if (POINTS[i].distance < mind)
            {
              mind = POINTS[i].distance;
              id = i;
            }
          }
          if (id > 0)
          {
            console.log("Cut off point!");
            CURPOINT = POINTS[id - 1].oid;
            POINTS.splice(0, id);
            if (!POINTS[1])
              this.$.turn.setSrc('assets/images/turns/finish.png');
            if (!POINTS[0])
            {
              this.$.compass.setSrc('assets/images/arrows/00.png');
              this.$.distance.setContent("Finished");
              return;
            }
            this.$.header.setContent("Turn " + (POINTS[0].id + 1) + " of " + NUMPOINTS);
          }
          break;
        case "aboutMenu":
          this.popup(ABOUT_TEXT);
          break;
      }
    }
  },
  popup: function(message)
  {
    this.$.popup.setContent(message);
    this.$.popup.show();
  },
  tback: function() {
    this.stopGeowatching();
    if (this.screenLock)
    {
      this.screenLock.unlock();
      this.screenLock = 0;
    }
    this.back();
  },
  setBack: function(f) {
    this.back = f;
  }
});

enyo.kind({
  name: "enyo.my.arrow",
  kind: "enyo.canvas.Shape",
  renderSelf: function(ctx) {
    var cwidth = ctx.canvas.width;
    var cheight = ctx.canvas.height;
    var width = 10;
    var height = 10;
    ctx.fillStyle = "";
    ctx.beginPath();
    ctx.moveTo(cwidth / 2, cheight * 0.7 - height / 2);
    ctx.lineTo(cwidth / 2 - width / 2, cheight * 0.7 + height);
    ctx.lineTo(cwidth / 2, cheight * 0.7 + height * 0.7);
    ctx.lineTo(cwidth / 2 + width / 2, cheight * 0.7 + height);
    ctx.lineTo(cwidth / 2, cheight * 0.7 - height / 2);
    //ctx.stroke();
    this.draw(ctx);
  },
  create: function() {
    this.inherited(arguments);
  },
  destroy: function() {
    this.inherited(arguments);
  }
});

enyo.kind({
  name: "enyo.my.track",
  kind: "enyo.canvas.Shape",
  renderSelf: function(ctx) {
    if (!TRACK[0])
      return;
    if (TRACK[1])
    {
      ctx.lineWidth = 3;
      ctx.fillStyle = "";
      var X = LOCATION.lon;
      var Y = LOCATION.lat;
      var x, y;
      var width = ctx.canvas.width * 128; //128 is scale
      var height = ctx.canvas.height * 128; // 128 is scale
      var ratio = width / height * MUL;
      var height = height * ratio;
      x = (TRACK[0].point.lon - X) * width;
      y = (TRACK[0].point.lat - Y) * height;
      ctx.beginPath();
      ctx.moveTo(x, -y);
      var pos;
      if (CURPOINT < TRACK.length)
        pos = CURPOINT;
      else
        pos = TRACK.length;
      ctx.strokeStyle = "rgb(0,250,0)";
      //TODO: Add bbox clipping;
      ctx.beginPath();
      for (i = 1; i <= pos; i++)
      {
        x = (TRACK[i].point.lon - X) * width;
        y = (TRACK[i].point.lat - Y) * height;
        ctx.lineTo(x, -y);
      }
      ctx.stroke();
      ctx.strokeStyle = "rgb(200,0,0)";
      ctx.fillStyle = "";
      ctx.beginPath();
      for (var i = pos; i < TRACK.length; i++)
      {
        x = (TRACK[i].point.lon - X) * width;
        y = (TRACK[i].point.lat - Y) * height;
        ctx.lineTo(x, -y);
        ctx.stroke();
      }
    }
    // Direction line
    x = (TRACK[0].point.lon - X) * width;
    y = (TRACK[0].point.lat - Y) * height;
    ctx.strokeStyle = "rgb(96,96,96)";
    ctx.lineWidth = 1;
    ctx.beginPath();
    ctx.moveTo(0, 0);
    ctx.lineTo(x, -y);
    ctx.stroke();
  },
  create: function() {
    this.inherited(arguments);
  },
  destroy: function() {
    this.inherited(arguments);
  }
});

enyo.kind({
  name: "enyo.my.building",
  kind: "enyo.canvas.Shape",
  renderSelf: function(ctx) {
    ctx.lineWidth = 2;
    ctx.beginPath();
    var X = LOCATION.lon;
    var Y = LOCATION.lat;
    var x, y;
    var width = ctx.canvas.width * 128;
    var height = ctx.canvas.height * 128;
    var ratio = width / height * MUL;

    ctx.fillStyle = "rgb(255,238,211)";
    ctx.lineWidth = 1;
    ctx.strokeStyle = "#d5b88c";
    var b = this.building;
    if (b.geometry.type !== 'Polygon')
      return;
    if (Math.abs(b.geometry.coordinates[0][0][0] - X) > 0.005 || //removing far building
            Math.abs(b.geometry.coordinates[0][0][1] - Y) > 0.005)
      return;
    //ctx.translate(width/2,height*0.7);
    //ctx.rotate(-ANGLE_DIRECTION);
    x = (b.geometry.coordinates[0][0][0] - X) * width;
    y = (b.geometry.coordinates[0][0][1] - Y) * height * ratio;
    ctx.beginPath();
    ctx.moveTo(x, -y);
    for (var j = 1; j < b.geometry.coordinates[0].length; j++)
    {
      x = (b.geometry.coordinates[0][j][0] - X) * width;
      y = (b.geometry.coordinates[0][j][1] - Y) * height * ratio;
      ctx.lineTo(x, -y);
    }
    ctx.closePath();
    ctx.fill();
    ctx.stroke();
    //ctx.restore();
    //this.draw(ctx);
  },
  create: function() {
    this.inherited(arguments);
  },
  destroy: function() {
    this.inherited(arguments);
  }
});

enyo.kind({
  name: "enyo.my.highway_primary",
  kind: "enyo.canvas.Shape",
  renderSelf: function(ctx) {
    ctx.lineWidth = 2;
    //ctx.save();
    var X = LOCATION.lon;
    var Y = LOCATION.lat;
    var x, y;
    var width = ctx.canvas.width * 128;
    var height = ctx.canvas.height * 128;
    var ratio = width / height * MUL;
    ctx.strokeStyle = '#909090';
    ctx.lineJoin = "round";
    ctx.lineCap = 'round';
    function draw() {
      ctx.beginPath();
      x = (h.geometry.coordinates[0][0] - X) * width;
      y = (h.geometry.coordinates[0][1] - Y) * height * ratio;
      ctx.moveTo(x, -y);
      for (var j = 1; j < h.geometry.coordinates.length; j++)
      {
        x = (h.geometry.coordinates[j][0] - X) * width;
        y = (h.geometry.coordinates[j][1] - Y) * height * ratio;
        ctx.lineTo(x, -y);
      }
      ctx.stroke();
    }
    var h = this.highway;
    if (h.properties.highway === "secondary" ||
            h.properties.highway === "secondary_link")
    {
      ctx.lineWidth = 10;
      ctx.strokeStyle = '#606060';
      draw();
      ctx.lineWidth = 8;
      ctx.strokeStyle = '#D3C32F';
      draw();
    }
    else if (h.properties.highway === "primary")
    {
      ctx.lineWidth = 10;
      ctx.strokeStyle = '#606060';
      draw();
      ctx.lineWidth = 8;
      ctx.strokeStyle = '#C09090';
      draw();
    }
    else if (h.properties.highway === "motorway" ||
            h.properties.highway === "motorway_link")
    {
      ctx.lineWidth = 10;
      ctx.strokeStyle = '#606060';
      draw();
      ctx.lineWidth = 8;
      ctx.strokeStyle = '#90C090';
      draw();
    }
    else if (h.properties.highway === "trunk" ||
            h.properties.highway === "trunk_link")
    {
      ctx.lineWidth = 10;
      ctx.strokeStyle = '#9BC3AE';
      draw();
      ctx.lineWidth = 8;
      ctx.strokeStyle = '#9BC3AE';
      draw();
    }
  },
  create: function() {
    this.inherited(arguments);
  },
  destroy: function() {
    this.inherited(arguments);
  }
});

enyo.kind({
  name: "enyo.my.highway",
  kind: "enyo.canvas.Shape",
  renderSelf: function(ctx) {
    ctx.lineWidth = 2;
    //ctx.save();
    var X = LOCATION.lon;
    var Y = LOCATION.lat;
    var x, y;
    var width = ctx.canvas.width * 128;
    var height = ctx.canvas.height * 128;
    var ratio = width / height * MUL;
    ctx.strokeStyle = '#909090';
    ctx.lineJoin = "round";
    ctx.lineCap = 'round';
    function draw() {
      ctx.beginPath();
      x = (h.geometry.coordinates[0][0] - X) * width;
      y = (h.geometry.coordinates[0][1] - Y) * height * ratio;
      ctx.moveTo(x, -y);
      for (var j = 1; j < h.geometry.coordinates.length; j++)
      {
        x = (h.geometry.coordinates[j][0] - X) * width;
        y = (h.geometry.coordinates[j][1] - Y) * height * ratio;
        ctx.lineTo(x, -y);
      }
      ctx.stroke();
    }
    var h = this.highway;
    if (h.properties.highway === "path")
    {
      ctx.lineWidth = 2;
      ctx.strokeStyle = '#cab19b';
    }
    else if (h.properties.highway === "footway")
    {
      ctx.lineWidth = 2;
      ctx.strokeStyle = '#cecece';
    }
    else if (h.properties.highway === "track")
    {
      ctx.lineWidth = 3;
      ctx.strokeStyle = '#c4a78b';
    }
    else
    {
      ctx.lineWidth = 5;
      ctx.strokeStyle = '#808080';
    }
    draw();
  },
  create: function() {
    this.inherited(arguments);
  },
  destroy: function() {
    this.inherited(arguments);
  }
});

enyo.kind({
  name: "enyo.my.water",
  kind: "enyo.canvas.Shape",
  renderSelf: function(ctx) {
    ctx.lineWidth = 2;
    ctx.beginPath();
    var X = LOCATION.lon;
    var Y = LOCATION.lat;
    var x, y;
    var width = ctx.canvas.width * 128;
    var height = ctx.canvas.height * 128;
    var ratio = width / height * MUL;

    ctx.fillStyle = "#bee1f6";
    ctx.strokeStyle = "#bee1f6";
    var b = this.water;
    ctx.beginPath();
    if (b.geometry.type === 'Polygon')
    {
      ctx.lineWidth = 1;
      x = (b.geometry.coordinates[0][0][0] - X) * width;
      y = (b.geometry.coordinates[0][0][1] - Y) * height * ratio;
      ctx.moveTo(x, -y);
      for (var j = 1; j < b.geometry.coordinates[0].length; j++)
      {
        x = (b.geometry.coordinates[0][j][0] - X) * width;
        y = (b.geometry.coordinates[0][j][1] - Y) * height * ratio;
        ctx.lineTo(x, -y);
      }
      ctx.closePath();
      ctx.fill();
      ctx.stroke();
    }
    else
    {
      ctx.lineWidth = 8;
      x = (b.geometry.coordinates[0][0] - X) * width;
      y = (b.geometry.coordinates[0][1] - Y) * height * ratio;
      ctx.moveTo(x, -y);
      for (j = 1; j < b.geometry.coordinates.length; j++)
      {
        x = (b.geometry.coordinates[j][0] - X) * width;
        y = (b.geometry.coordinates[j][1] - Y) * height * ratio;
        ctx.lineTo(x, -y);
      }
      ctx.stroke();
    }

  },
  create: function() {
    this.inherited(arguments);
  },
  destroy: function() {
    this.inherited(arguments);
  }
});




enyo.kind({
  name: "enyo.my.prepare",
  kind: "enyo.canvas.Shape",
  renderSelf: function(ctx) {
    ctx.save();
    var width = ctx.canvas.width;
    var height = ctx.canvas.height;
    ctx.translate(width / 2, height * 0.7);
    ctx.rotate(ANGLE_DIRECTION);
    //this.draw(ctx);
  },
  create: function() {
    this.inherited(arguments);
  },
  destroy: function() {
    this.inherited(arguments);
  }
});

enyo.kind({
  name: "enyo.my.finish",
  kind: "enyo.canvas.Shape",
  renderSelf: function(ctx) {
    ctx.restore();
    //this.draw(ctx);
  },
  create: function() {
    this.inherited(arguments);
  },
  destroy: function() {
    this.inherited(arguments);
  }
});
