
var I = 0;
var MAP;
var newData = 0;
function getMap(track, progress)
{
  var bbox = {};
  MAP = 0;
  newData = 0;
  if (track[0])
  {
    bbox.lat1 = bbox.lat2 = Number(track[0].point.lat);
    bbox.lon1 = bbox.lon2 = Number(track[0].point.lon);
  }
  bbox.lat1 = Number(bbox.lat1);
  bbox.lat2 = Number(bbox.lat2);
  bbox.lon1 = Number(bbox.lon1);
  bbox.lon2 = Number(bbox.lon2);
  for (var i = 1; i < track.length; i++)
  {
    p = track[i].point;
    p.lat = Number(p.lat);
    p.lon = Number(p.lon);
    if (p.lat < bbox.lat1)
      bbox.lat1 = p.lat;
    if (p.lon < bbox.lon1)
      bbox.lon1 = p.lon;
    if (p.lat > bbox.lat2)
      bbox.lat2 = p.lat;
    if (p.lon > bbox.lon2)
      bbox.lon2 = p.lon;
  }
  console.log("bbox=");
  bbox.lat1 = Number(Number(bbox.lat1).toFixed(2));
  bbox.lat2 = Number(Number(bbox.lat2).toFixed(2));
  bbox.lon1 = Number(Number(bbox.lon1).toFixed(2));
  bbox.lon2 = Number(Number(bbox.lon2).toFixed(2));
  console.log(bbox);

  var requests = [];
  var dd = calcDistance(0.5, track[0].point);//km Cannot be more 2 km!
  dd.lat = Number(dd.lat.toFixed(4));
  dd.lon = Number(dd.lon.toFixed(4));
  console.log(dd);
  for (var y = bbox.lat1; y < bbox.lat2 + dd.lat*2; y += dd.lat)
    for (var x = bbox.lon1; x < bbox.lon2 + dd.lon*2; x += dd.lon)
    {
      var req = {};
      req.lat1 = y - dd.lat;
      req.lat2 = y;
      req.lon1 = x - dd.lon;
      req.lon2 = x;
      req.hash = req.lat1.toFixed(4) + "_" + req.lon1.toFixed(4);
      requests.push(req);
    }
  console.log("needed " + requests.length + " requests");
  for (i = 0; i < track.length; i++)
  {
    var p = track[i].point;
    p.lat = Number(p.lat);
    p.lon = Number(p.lon);
    for (var j = 0; j < requests.length; j++)
    {
      var r = requests[j];
      if ((p.lat > r.lat1) && (p.lat < r.lat2) &&
              (p.lon > r.lon1) && (p.lon < r.lon2))
      {
        requests[j].valid = 1;
      }
    }
  }
  for (i = 0; i < requests.length; i++)
  {
    if (!requests[i].valid)
    {
      requests.splice(i, 1);
      i--;
    }
  }
  console.log("trying to get cache");

  var onreadystatechange = function(a) {
    if (a.target.readyState === 4) {
      if (a.target.status === 200) {
        I++;
        var r = a.target.request;
        if (!r) //google chrome!
        {
          console.log("Seems like it's google chrome. God save the following code");
          //"http://api.openstreetmap.org/api/0.6/map?bbox=37.8505,55.9355,37.858599999999996,55.94"
          var h = a.target.responseXML.URL.match(/bbox=([\d\.]+),([\d\.]+)/);
          console.log(h);
          r = {};
          r.hash = h[1] + "_" + h[2];
        }
        var t = a.target.responseText;
        var part = osm2geo(t);
        console.log("r=" + r.hash);
        //console.log("target=" + a.target.request.hash);
        if (!MAP) {
          MAP = part;
          MAP.hashes = [];
          //MAP.hashes.push(a.target.request.hash);
          MAP.hashes.push(r.hash);
        }
        else {
          var m = MAP.features;
          var p = part.features;
          for (var i = 0; i < p.length; i++) {
            var flag = 0;
            for (var j = 0; j < m.length; j++) {
              if (m[j].id === p[i].id) {
                flag = 1;
                break;
              }
            }
            if (!flag) {
              MAP.features.push(p[i]);
            }

          }
        }
        MAP.hashes.push(r.hash);
        progress(I, requests.length);
        console.log("downloaded chunk " + (I) + "/" + requests.length);
        console.log("MAP objects:" + MAP.features.length);
      } else {
        switch (a.target.status) {
          case 0:
            console.log("Cannot connect to the server");
            break;
          default:
            console.log(a.target.statusText);
            break;
        }
      }
    }
  };


  function getosmdata()
  {
    var httpreq = [];
    I = 0;
    for (i = 0; i < requests.length; i++)
    {
      r = requests[i];
      var flag = 0;
      if (MAP && MAP.hashes)
        for (var j = 0; j < MAP.hashes.length; j++)
        {
          if (MAP.hashes[j] === requests[i].hash)
          {
            flag = 1;
            console.log("cache hit");
            break;
          }
        }
      if (flag)
      {
        I++;
        continue;
      }
      console.log("Cache miss. Hash=" + r.hash);
      var url = "http://api.openstreetmap.org/api/0.6/map?bbox=" + r.lon1 + "," + r.lat1 + "," + r.lon2 + "," + r.lat2;
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = onreadystatechange;
      xmlhttp.id = i;
      xmlhttp.request = requests[i];
      //xmlhttp.open('GET', url, true);
      httpreq.push(xmlhttp);
    }
    if (httpreq.length < 1)
    {
      progress(1, 1);
    } else {
      newData = 1;
    }
    for (var i = 0; i < httpreq.length; i++)
    {
      var r = httpreq[i].request;
      console.log("hash=" + r.hash);
      url = "http://api.openstreetmap.org/api/0.6/map?bbox=" + r.lon1 + "," + r.lat1 + "," + r.lon2 + "," + r.lat2;
      httpreq[i].open('GET', url, true);
      httpreq[i].send();
    }

  }
  if (navigator.getDeviceStorage) //get map cache
  {
    var storage = navigator.getDeviceStorage("sdcard");
    var request = storage.get('trackguide.mapchache');
    request.onsuccess = function() {
      var name = this.result.name;
      console.log('File "' + name + '" successfully retrieved from the sd card storage area');
      var reader = new FileReader();
      reader.readAsText(this.result);
      //var res=this.result;
      reader.onload = function()
      {
        MAP = JSON.parse(this.result);
        getosmdata();
      };
      reader.onerror = function()
      {
        console.log("Cannot read file");
        getosmdata();
      };
    };
    request.onerror = function() {
      console.warn('Unable to get the file: ' + this.error);
      getosmdata();
    };
  }
  else
  {
    getosmdata();
  }
  console.log("valid requests: " + requests.length);


}

function doneMapDownloading()
{
  //saving cache
  if (navigator.getDeviceStorage && newData)
  {
    var sdcard = navigator.getDeviceStorage("sdcard");
    var request = sdcard.delete("trackguide.mapchache");
    request.onsuccess = function() {
      var file = new Blob([JSON.stringify(MAP)], {type: "text/plain"});
      var request = sdcard.addNamed(file, "trackguide.mapchache");
      request.onsuccess = function() {
        var name = this.result;
        done(MAP);//going to mainmap.js
        console.log('File "' + name + '" successfully wrote on the sdcard storage area');
      };
      // An error typically occur if a file with the same name already exist
      request.onerror = function() {
        console.warn('Unable to write the file: ' + this.error);
        done(MAP);//going to mainmap.js
      };
    };
    request.onerror = function() {
      console.log("Unable to delete the file: " + this.error);
      var file = new Blob([JSON.stringify(MAP)], {type: "text/plain"});
      var request = sdcard.addNamed(file, "trackguide.mapchache");
      request.onsuccess = function() {
        var name = this.result;
        done(MAP);//going to mainmap.js
        console.log('File "' + name + '" successfully wrote on the sdcard storage area');
      };
      // An error typically occur if a file with the same name already exist
      request.onerror = function() {
        console.warn('Unable to write the file: ' + this.error);
        done(MAP);//going to mainmap.js
      };
    };
  }
  else {
    done(MAP);//going to mainmap.js
  }
}