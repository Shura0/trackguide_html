function Point(lat, lon)
{
  this.lat = lat;
  this.lon = lon;
}

Point.prototype.setPosition = function(lat, lon)
{
  this.lat = lat;
  this.lon = lon;
};

function Target(lat, lon, description)
{
  this.point = new Point(lat, lon);
  this.id = 0; //id after simplyfication
  this.oid = 0; //original id
  this.distance = 0;
  this.direction = 0;
  this.flags = {}; //reserved
  this.image = "assets/images/nogps.png";
  return this;
}

Target.prototype.copyFrom = function(t)
{
  this.direction = t.direction;
  this.distance = t.distance;
  this.flags = t.flags;
  this.id = t.id;
  this.image = t.image;
//  this.point={};
  this.point.lat = t.point.lat;
  this.point.lon = t.point.lon;
  return this;
};


Target.prototype.setPosition = function(lat, lon)
{
  this.point.setPosition(lat, lon);
};

Target.prototype.calcDistance = function(position)
{
  this.distance = Distance(position, this.point);
};

Target.prototype.calcDirection = function(curpos, curdir)
{
  var p = new Point(this.point.lat, this.point.lon);
  p.lat -= curpos.lat;
  p.lon -= curpos.lon;
  if ((curdir.lat === 0) && (curdir.lon === 0))
  {
    this.image2 = "assets/images/arrows/00.png";
    this.image = "assets/images/nogps.png";
    this.dir = -1;
    return;
  }
  var d = Angle(p, curdir);
  this.direction = d;
  var step = Math.PI / 8;
  var imageid = Math.round(d / step + step / 2);
  imageid = -imageid;
  if (imageid < 0)
  {
    imageid = 16 + imageid;
  }
  this.dir = imageid;
  this.image2 = "assets/images/arrows/" + imageid + ".png";
  this.image = "assets/images/turns/" + imageid + ".png";


};

function Distance(p1, p2)
{
  var lat1 = p1.lat;
  var lat2 = p2.lat;
  var lon1 = p1.lon;
  var lon2 = p2.lon;
  var R = 6378.137; // Radius of earth in KM
  var dLat = (lat2 - lat1) * Math.PI / 180;
  var dLon = (lon2 - lon1) * Math.PI / 180;
  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
          Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
          Math.sin(dLon / 2) * Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return Math.round(d * 1000); // kilometers
}

function Angle(a, b)
{
  var angle = Math.atan2(a.lat * b.lon - b.lat * a.lon, a.lat * b.lat + a.lon * b.lon);
  return angle;

}

function normalize(p)
{
  var len = Math.sqrt(p.lat * p.lat + p.lon * p.lon);
  p.lat /= len;
  p.lon /= len;
  return p;
}

function calcDirection(oldd, newd)
{
  oldd.lat += newd.lat;
  oldd.lon += newd.lon;
  normalize(oldd);
  return oldd;
}

function simpleTrack(track, level)
{
  if (!level)
  {
    for (var i = 1; i < track.length; i++)
    {
      if ((!track[i]) || (!track[i]))
        break;
      if (Distance(track[i].point, track[i - 1].point) < 15) //meters
      {
        track.splice(i, 1);
        i--;
      }
    }
  }
  else
    for (var i = 1; i < track.length; i++)
    {
      if ((!track[i]) || (!track[i]))
        break;
      if (Distance(track[i].point, track[i - 1].point) < 5) //meters
      {
        track.splice(i, 1);
        i--;
      }
    }
  var flag;
  var t = 10;
  if (!level)
  {
    do {
      flag = 0;
      for (var i = 1; i < track.length - 1; i++)
      {
        if ((!track[i]) || (!track[i + 1]))
        {
          break;
        }
        var curd = new Point(0, 0);
        curd.lon = track[i].point.lon - track[i - 1].point.lon;
        curd.lat = track[i].point.lat - track[i - 1].point.lat;

        var nextd = new Point(0, 0);
        nextd.lon = track[i + 1].point.lon - track[i].point.lon;
        nextd.lat = track[i + 1].point.lat - track[i].point.lat;

        var a = Angle(curd, nextd);
        if (Math.abs(a) < 0.25) //remove weak turns
        {
          track.splice(i, 1);
          i--;
          flag = 1;
          continue;
        }
      }
      t--;
    } while (flag && (t > 0));
  }
  else
  {
    do {
      flag = 0;
      for (var i = 1; i < track.length - 1; i++)
      {
        if ((!track[i]) || (!track[i + 1]))
        {
          break;
        }
        var curd = new Point(0, 0);
        curd.lon = track[i].point.lon - track[i - 1].point.lon;
        curd.lat = track[i].point.lat - track[i - 1].point.lat;

        var nextd = new Point(0, 0);
        nextd.lon = track[i + 1].point.lon - track[i].point.lon;
        nextd.lat = track[i + 1].point.lat - track[i].point.lat;

        var a = Angle(curd, nextd);
        if (Math.abs(a) < 0.10) //remove weak turns
        {
          track.splice(i, 1);
          i--;
          flag = 1;
          continue;
        }
      }
      t--;
    } while (flag && (t > 0));
  }
  if (level)
    for (var i = 0; i < track.length; i++)
    {
      track[i].oid = i;
    }
  else
    for (var i = 0; i < track.length; i++)
    {
      track[i].id = i;
    }
}
