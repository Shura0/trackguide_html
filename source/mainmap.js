// http://api.openstreetmap.org/api/0.6/map?bbox=37.8199,56.006,37.8424,56.0122

//'http://api.openstreetmap.org/api/0.6/map?bbox=37.82114,56.00883,37.82356,56.01051'


//http://api.openstreetmap.org/api/0.6/map?bbox=37.8436,55.9873,37.8675,55.9984

//http://openstreetmap.ru/#zoom=17&lat=55.994528&lon=37.85392&marker=1



//VERY BIG!!! http://api.openstreetmap.org/api/0.6/map?bbox=37.8138,55.9969,37.8808,56.0229


//var OBJ;
var POSITION = new Point(56.010324, 37.8254);
//var POSITION=new Point(55.9945,37.85392);
var C;
var MOUSE_DOWN = 0;
var CURSOR = new Point(256, 256);
var DELTA = new Point(0, 0);
var ZOOM = 10000;
var NEED_REFRESH = 0;
var ANGLE = 0;


function drawAll()
{
  if (!NEED_REFRESH)
  {
    NEED_REFRESH = 1;
    setTimeout(redraw, 0);
  }
}

function redraw()
{
  if (NEED_REFRESH)
  {
    clear();
    preDraw(CURSOR);
    drawGrasses();
    drawWoods();
    drawWaters();
    drawBuildings();
    drawHighways();
    drawRailways();
    drawHousenumbers();
    endDraw();

    //setTimeout(redraw,20);
    NEED_REFRESH = 0;
    //ANGLE+=0.0005;

  }
}

function mousemove(event)
{
  if (!MOUSE_DOWN)
    return;
  var dx = event.clientX - DELTA.lon;
  var dy = event.clientY - DELTA.lat;
  DELTA.lon = event.clientX;
  DELTA.lat = event.clientY;
  CURSOR.lon += dx;
  CURSOR.lat += dy;
  drawAll();
}

function mousedown(event)
{
  if (event.button === 0)
  {
    MOUSE_DOWN = 1;
    DELTA.lon = event.clientX;
    DELTA.lat = event.clientY;
  }
}

function mouseup(event)
{
  if (event.button === 0)
    MOUSE_DOWN = 0;
}

function mousewheel(event)
{
  //console.log("event:"+event.detail);
  if (event.detail > 0)
    ZOOM -= 100;
  else
    ZOOM += 100;
  drawAll();
}


function done(obj)
{
  getBuildings(obj);
  getHighways(obj);
  getRailways(obj);
  getWoods(obj);
  getWaters(obj);
  //getGrasses(obj);
  //drawAll();
}

function main()
{
  initCanvas(document.getElementById('screen'));
  console.log(data_json.length);
  document.getElementById('screen').addEventListener('mousemove', mousemove, true);
  document.getElementById('screen').addEventListener('mousedown', mousedown, true);
  document.getElementById('screen').addEventListener('mouseup', mouseup, true);
  document.getElementById('screen').addEventListener('DOMMouseScroll', mousewheel, true);

  loadSettings(done);
}
