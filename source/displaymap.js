

var _c;
var buildings = [];
var highways = [];
var railways = [];
var woods = [];
var waters = [];
var grasses = [];
var cur;

function initCanvas(screen)
{
  _c = screen.getContext('2d');
}

function preDraw(p)
{
  cur = p;
  _c.save();
  _c.translate(cur.lon, cur.lat);
  _c.rotate(ANGLE);
  _c.rect(-5, -5, 10, 10);
  _c.stroke();
}

function endDraw()
{
  _c.restore();
}

function setBuildings(b)
{
  buildings = b;
}

function getBuildings(json)
{
  buildings = [];
  if (!json && !json.features)
    return;
  for (var i = 0; i < json.features.length; i++)
  {
    var a = json.features[i];
    if (a.properties && a.properties.building)
    {

      var minlat = 256;
      var minlon = 256;
      var maxlat = -256;
      var maxlon = -256;
      for (var j = 0; j < a.geometry.coordinates[0].length; j++)
      {
        var c = a.geometry.coordinates[0][j];
        if (c[0] < minlon)
          minlon = c[0];
        if (c[1] < minlat)
          minlat = c[1];
        if (c[0] > maxlon)
          maxlon = c[0];
        if (c[1] > maxlat)
          maxlat = c[1];
      }
      a.geometry.textposition = [];
      a.geometry.textposition[0] = (minlon + maxlon) / 2 - 0.0001;
      a.geometry.textposition[1] = (minlat + maxlat) / 2;
      buildings.push(a);
      //console.log("Building "+a.properties.building);
    }
  }
  console.log("found " + buildings.length + " buildings");
  return buildings;
}

function getWoods(json)
{
  woods = [];
  if (!json && !json.features)
    return;
  for (var i = 0; i < json.features.length; i++)
  {
    var a = json.features[i];
    if (a.properties && a.properties.natural &&
            (a.properties.natural === 'wood' ||
                    a.properties.natural === 'scrub'))
    {
      woods.push(a);
    }
  }
  console.log("found " + woods.length + " wood");
}

function getWaters(json)
{
  waters = [];
  if (!json && !json.features)
    return;
  for (var i = 0; i < json.features.length; i++)
  {
    var a = json.features[i];
    if (a.properties && a.properties.waterway ||
            (a.properties.natural &&
                    (a.properties.natural === 'water' /*||
                     a.properties.natural=='wetland'*/)))
    {
      waters.push(a);
    }
  }
  console.log("found " + waters.length + " water");
}

function getGrasses(json)
{
  if (!json && !json.features)
    return;
  for (var i = 0; i < json.features.length; i++)
  {
    var a = json.features[i];
    if (a.properties &&
            ((a.properties.natural && a.properties.natural === 'grassland') ||
                    a.properties.landuse && a.properties.landuse === 'meadow'))
    {
      grasses.push(a);
    }
  }
  console.log("found " + waters.length + " grasses");
}

function getHighways(json)
{
  highways = [];
  if (!json && !json.features)
    return;
  for (var i = 0; i < json.features.length; i++)
  {
    var a = json.features[i];
    if (a.properties && a.properties.highway &&
            (
                    a.properties.highway === "path" ||
                    a.properties.highway === "footway" ||
                    a.properties.highway === "track" ||
                    a.properties.highway === "residential" ||
                    a.properties.highway === "service" ||
                    a.properties.highway === "tertiary" ||
                    a.properties.highway === "secondary" ||
                    a.properties.highway === "trunk" ||
                    a.properties.highway === "trunk_link" ||
                    a.properties.highway === "primary" ||
                    a.properties.highway === "secondary_link"))
    {
      highways.push(a);
      //console.log("highway "+a.properties.highway);
    }
  }
  console.log("found " + highways.length + " highways");
}

function getRailways(json)
{
  railways = [];
  if (!json && !json.features)
    return;
  for (var i = 0; i < json.features.length; i++)
  {
    var a = json.features[i];
    if (a.properties && a.properties.railway)
    {
      railways.push(a);
      //console.log("highway "+a.properties.highway);
    }
  }
  console.log("found " + railways.length + " railways");
}


function drawBuildings()
{
  _c.fillStyle = "#ffeed3";
  _c.lineWidth = 1;
  _c.strokeStyle = "#d5b88c";
  //console.log("DrawBuilings");
  //console.log("cur="+cur.lon+" "+cur.lat);
  for (var i = 0; i < buildings.length; i++)
  {
    var b = buildings[i];
    if (b.geometry.type !== 'Polygon')
      continue;
    _c.beginPath();
    //console.log("beginPath");
    var x = b.geometry.coordinates[0][0][0] - POSITION.lon;
    var y = b.geometry.coordinates[0][0][1] - POSITION.lat;
    x *= 25 * ZOOM;
    y *= -50 * ZOOM;
    //console.log("moveTo "+x+" , "+y);
    _c.moveTo(x, y);
    for (var j = 1; j < b.geometry.coordinates[0].length; j++)
    {
      x = b.geometry.coordinates[0][j][0] - POSITION.lon;
      y = b.geometry.coordinates[0][j][1] - POSITION.lat;
      x *= 25 * ZOOM;
      y *= -50 * ZOOM;
      //console.log("lineTo "+x+" , "+y);
      _c.lineTo(x, y);
    }
    _c.closePath();
    _c.fill();
    _c.stroke();
    //console.log("closePath");
  }
}

function drawHousenumbers()
{
  _c.font = '11px Arial';
  _c.fillStyle = "black";
  _c.rotate(-ANGLE);

  for (var i = 0; i < buildings.length; i++)
  {
    var b = buildings[i];
    if (b.geometry.type !== 'Polygon')
      continue;
    if (b.properties.hasOwnProperty('addr:housenumber'))
    {
      //var x=b.geometry.coordinates[0][0][0]-POSITION.lon;
      //var y=b.geometry.coordinates[0][0][1]-POSITION.lat;
      var x = b.geometry.textposition[0] - POSITION.lon;
      var y = b.geometry.textposition[1] - POSITION.lat;
      x *= 25 * ZOOM;
      y *= -50 * ZOOM;
      _c.save();
      _c.translate(-x, -y);
      _c.rotate(ANGLE);
      _c.translate(x, y);
      _c.rotate(-ANGLE);
      _c.fillText(b.properties['addr:housenumber'], x, y);
      _c.restore();
    }
  }
  // _c.restore();
}

function drawHighways()
{
  _c.strokeStyle = '#808080';
  _c.lineJoin = "round";
  _c.lineCap = 'round';
  for (var i = 0; i < highways.length; i++)
  {
    var h = highways[i];
    if(h.properties.highway === "trunk")
    {
      _c.lineWidth = 30 * ZOOM / 10000 + 4;
      _c.strokeStyle = '#9BC3AE';
    }
    if (h.properties.highway === "secondary" ||
            h.properties.highway === "secondary_link")
    {
      _c.lineWidth = 30 * ZOOM / 10000 + 2;
      _c.strokeStyle = '#707070';
    }
    else if (h.properties.highway === "path")
    {
      _c.lineWidth = 2;
      _c.strokeStyle = '#cab19b';
    }
    else if (h.properties.highway === "footway")
    {
      _c.lineWidth = 2;
      _c.strokeStyle = '#cecece';
    }
    else if (h.properties.highway === "track")
    {
      _c.lineWidth = 3;
      _c.strokeStyle = '#c4a78b';
    }
    else
    {
      _c.lineWidth = 10 * ZOOM / 10000 + 1;
      _c.strokeStyle = '#808080';
    }
    _c.beginPath();
    var x = h.geometry.coordinates[0][0] - POSITION.lon;
    var y = h.geometry.coordinates[0][1] - POSITION.lat;
    x *= 25 * ZOOM;
    y *= -50 * ZOOM;
    _c.moveTo(x, y);
    for (var j = 1; j < h.geometry.coordinates.length; j++)
    {
      x = h.geometry.coordinates[j][0] - POSITION.lon;
      y = h.geometry.coordinates[j][1] - POSITION.lat;
      x *= 25 * ZOOM;
      y *= -50 * ZOOM;
      _c.lineTo(x, y);
    }
    _c.stroke();
  }
}

function drawRailways()
{
  _c.lineWidth = 2 * ZOOM / 10000;
  _c.strokeStyle = 'red';
  _c.lineJoin = "round";
  _c.lineCap = 'round';
  for (var i = 0; i < railways.length; i++)
  {
    var h = railways[i];
    _c.beginPath();
    var x = h.geometry.coordinates[0][0] - POSITION.lon;
    var y = h.geometry.coordinates[0][1] - POSITION.lat;
    x *= 25 * ZOOM;
    y *= -50 * ZOOM;
    _c.moveTo(x, y);
    for (var j = 1; j < h.geometry.coordinates.length; j++)
    {
      x = h.geometry.coordinates[j][0] - POSITION.lon;
      y = h.geometry.coordinates[j][1] - POSITION.lat;
      x *= 25 * ZOOM;
      y *= -50 * ZOOM;
      _c.lineTo(x, y);
    }
    _c.stroke();
  }
}


function drawWoods()
{
  _c.fillStyle = "#d0e4b7";
  _c.lineWidth = 1;
  _c.strokeStyle = "#d0e4b7";
  for (var i = 0; i < woods.length; i++)
  {
    var b = woods[i];
    _c.beginPath();
    var x = b.geometry.coordinates[0][0][0] - POSITION.lon;
    var y = b.geometry.coordinates[0][0][1] - POSITION.lat;
    x *= 25 * ZOOM;
    y *= -50 * ZOOM;
    _c.moveTo(x, y);
    for (var j = 1; j < b.geometry.coordinates[0].length; j++)
    {
      x = b.geometry.coordinates[0][j][0] - POSITION.lon;
      y = b.geometry.coordinates[0][j][1] - POSITION.lat;
      x *= 25 * ZOOM;
      y *= -50 * ZOOM;
      _c.lineTo(x, y);
    }
    _c.closePath();
    _c.fill();
    _c.stroke();
  }
}

function drawGrasses()
{
  _c.fillStyle = "#e4f0d2";
  _c.lineWidth = 1;
  _c.strokeStyle = "#e4f0d2";
  for (var i = 0; i < grasses.length; i++)
  {
    var b = grasses[i];
    _c.beginPath();
    var x = b.geometry.coordinates[0][0][0] - POSITION.lon;
    var y = b.geometry.coordinates[0][0][1] - POSITION.lat;
    x *= 25 * ZOOM;
    y *= -50 * ZOOM;
    _c.moveTo(x, y);
    for (var j = 1; j < b.geometry.coordinates[0].length; j++)
    {
      x = b.geometry.coordinates[0][j][0] - POSITION.lon;
      y = b.geometry.coordinates[0][j][1] - POSITION.lat;
      x *= 25 * ZOOM;
      y *= -50 * ZOOM;
      _c.lineTo(x, y);
    }
    _c.closePath();
    _c.fill();
    _c.stroke();
  }
}



function drawWaters()
{
  radius = 10;
  _c.fillStyle = "#bee1f6";
  _c.strokeStyle = "#bee1f6";
  for (var i = 0; i < waters.length; i++)
  {
    var b = waters[i];
    _c.beginPath();
    if (b.geometry.type == 'Polygon')
    {
      _c.lineWidth = 1;
      var x = b.geometry.coordinates[0][0][0] - POSITION.lon;
      var y = b.geometry.coordinates[0][0][1] - POSITION.lat;
      x *= 25 * ZOOM;
      y *= -50 * ZOOM;
      //console.log("moveTo "+x+" , "+y);
      _c.moveTo(x, y);
      for (var j = 1; j < b.geometry.coordinates[0].length; j++)
      {
        x = b.geometry.coordinates[0][j][0] - POSITION.lon;
        y = b.geometry.coordinates[0][j][1] - POSITION.lat;
        x *= 25 * ZOOM;
        y *= -50 * ZOOM;
        //console.log("lineTo "+x+" , "+y);
        _c.lineTo(x, y);
      }
      _c.closePath();
      _c.fill();
      _c.stroke();

    }
    else
    {
      _c.lineWidth = 8;
      var x = b.geometry.coordinates[0][0] - POSITION.lon;
      var y = b.geometry.coordinates[0][1] - POSITION.lat;
      x *= 25 * ZOOM;
      y *= -50 * ZOOM;
      //console.log("moveTo "+x+" , "+y);
      _c.moveTo(x, y);
      for (var j = 1; j < b.geometry.coordinates.length; j++)
      {
        x = b.geometry.coordinates[j][0] - POSITION.lon;
        y = b.geometry.coordinates[j][1] - POSITION.lat;
        x *= 25 * ZOOM;
        y *= -50 * ZOOM;
        //console.log("lineTo "+x+" , "+y);
        _c.lineTo(x, y);
      }
      //_c.closePath();
      //_c.fill();
      _c.stroke();
    }
    //console.log("closePath");
  }
}


function clear()
{
  _c.width = _c.width;
  _c.clearRect(0, 0, 512, 512);
}