//var TARGETS=[];
var POINTS=[];
var FILES=[];
var TRACK=[];
var LOCATION=new Point(0,0);
//var LOCATION=new Point(55.8523,37.6473);
var DIRECTION=new Point(1,0);
var ANGLE_DIRECTION;
//lat=55.8523&lon=37.6473
var WRONGDIRECTION=0;
var NUMPOINTS=0;
var CURPOINT=0;
var MUL=1;
var REQUEST='http://api.openstreetmap.org/api/0.6/notes.json?bbox='+(LOCATION.lon-1)+','+
(LOCATION.lat-1)+','+(LOCATION.lon+1)+','+(LOCATION.lat+1)+'&closed=0';

//===================
var VERSION="v0.3.92";
var PAYPAL_BUTTON='<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=shura0%40yandex%2eru&lc=RU&item_name=Alexander%20Zaitsev&item_number=trackguide&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donate_CC_LG%2egif%3aNonHosted" target=_blank><img src="https://www.paypalobjects.com/en_US/RU/i/btn/btn_donateCC_LG.gif"></a>';
var ABOUT_TEXT='<div style="text-align:center"><b>Track Giude '+VERSION+'</b><br></div><div>The program is written by Alexander Zaitsev <a href="mailto:shura0@yandex.ru">shura0@yandex.ru</a> and distributed under BSD license.<br>\
If you like the program please donate<br></div><div style="text-align:center">'+PAYPAL_BUTTON+"</div>";

var SETTINGS={
  };

var DISPLAYMODE='alwayson';

var SHIFT=new Point(0,0);

cloneObject=function(source) {
  for (i in source) {
    if (typeof source[i] === 'source') {
        this[i] = new cloneObject(source[i]);
    }
    else{
        this[i] = source[i];
    }
  }
};


function saveSettings() {

  document.cookie="settings="+JSON.stringify(SETTINGS);
}

function loadSettings(f) {//callback function
  console.log(document.cookie);
  var str=document.cookie.split("=")[1];
  if(!str) return;
  SETTINGS=JSON.parse(str);
  console.log(SETTINGS);
}

function parseGPX(data)
{
  if (window.DOMParser)
  {
    console.log("XML parser!");
    this.parser=new DOMParser();
    this.xmlDoc=this.parser.parseFromString(data,"text/xml");
    var points=this.xmlDoc.getElementsByTagName("trkpt");
    if(points.length<1)
    {
      console.log("Cannot find 'trkpt' entries, trying to find 'route points'");
      points=this.xmlDoc.getElementsByTagName("rtept");
    }
    if(points.length<1)
    {
      console.log("Cannot find 'rtept' entries, trying to find 'waypoints'");
      points=this.xmlDoc.getElementsByTagName("wpt");
    }
    if(points.length<1)
    {
      console.log("Cannot find waypoints. Break");
      return 0;
    }
    console.log("found %i points",points.length);
    POINTS=[];
    TRACK=[];
    for(var i=0;i<points.length;i++)
    {
      var lat=0;
      var lon=0;
      for(var j=0;j<points[i].attributes.length;j++)
      {
        if(points[i].attributes[j].name.toUpperCase() ==='LAT')
          lat=points[i].attributes[j].value;
        else
        if(points[i].attributes[j].name.toUpperCase() ==='LON')
          lon=points[i].attributes[j].value;
      }
      var point=new Target(lat,lon);
      point.oid=i;
      //POINTS.push(point);
      TRACK.push(point);
    }
    console.log("Track simplification...");
    simpleTrack(TRACK,1);
    POINTS=POINTS.concat(TRACK);
    simpleTrack(POINTS);
    console.log("Done. "+POINTS.length+" turns left");
    console.log("Done. "+TRACK.length+" points in track left");
    if(POINTS[0]) //get difference in one km between lat and lon
    {
      var dd=calcDistance(1,TRACK[0].point);
      MUL=dd.lon/dd.lat;
    }
    NUMPOINTS=POINTS.length;
    return NUMPOINTS;
  }
}


function toHuman(size)
{
  if(size<=1024) return size;
  if(size > 1024 && size < 1024*1024)
    return (size/1024).toFixed(2) + " Kb";
  else
    return (size/1024/1024).toFixed(2) + " Mb";
}


function calcDistance(distance, loc)
{
  if(!loc)
    loc=LOCATION;
  var dlat=distance/111; //km per degree latitude
  var dlon=distance/(111*Math.cos(loc.lat * Math.PI / 180));
  return {lat:dlat,lon:dlon};
  //return 'http://api.openstreetmap.org/api/0.6/notes.json?bbox='+(LOCATION.lon-dlon)+','+(LOCATION.lat-dlat)+','+(LOCATION.lon+dlon)+','+(LOCATION.lat+dlat)+'&closed=0';
}
