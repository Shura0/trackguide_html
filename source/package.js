enyo.depends(
	"$lib/layout",
	"$lib/onyx",	// To theme Onyx using Theme.less, change this line to $lib/onyx/source,
  "$lib/canvas",
	//"Theme.less",	// uncomment this line, and follow the steps described in Theme.less
  "global.js",
  "osmutil.js",
  "track.js",
  "point.js",
  "guide.js",
  "files.js",
  "mainmap.js",
  "displaymap.js",
  "jquery.js",
  "xmltojson.js",
	"App.css",
	"App.js"
);
