enyo.kind({
  name: 'FilesScene',
  kind: 'FittableRows',
  classes: "enyo-fit",
  components: [
    {kind: "onyx.Toolbar", components: [
        {components: [
            {kind: "FittableColumns", classes: "enyo-fit", style: "padding:6px", components: [
                //{kind: "onyx.IconButton", src:"assets/images/add-icon.png", ontap:"download"},
                {fit: true, content: "Open track", style: "text-align:center"},
                {kind: "onyx.MenuDecorator", onSelect: "menuSelected", components: [
                    {kind: "onyx.IconButton", src: "assets/images/menu.png"},
                    {kind: "onyx.Menu", floating: true, components: [
                        {content: "Refresh file list", name: "refreshMenu"},
                        {name: "mapMenu", components: [
                            {kind: "onyx.IconButton", src: "assets/images/checkbox_unchecked.png", name: "mapMenuIcon"},
                            {content: "Download map"}
                          ]},
                        {classes: "onyx-menu-divider"},
                        {content: "About", name: "aboutMenu"}
                      ]}
                  ]}
              ]}
          ]}
      ]},
    {name: "popup", kind: 'onyx.Popup', centered: true, floating: true, modal: true,
      allowHtml: true, style: "padding: 10px;"},
    {name: "mapdownloadingpopup", kind: 'onyx.Popup', centered: true, floating: true, modal: true,
      allowHtml: true, style: "padding: 10px;width:89%", components: [
        {content: "Map downloading & processing", classes: "popup"},
        {kind: "onyx.ProgressButton", name: "mapprogress", showStripes: false, progress: 0, onCancel: "cancelMapDownloading"}
      ]},
    {kind: "List", name: "list", fit: true, classes: "bk", noSelect: true, onSetupItem: "setupItem", fixedHeight: true, components: [
        {name: "item", style: "padding: 10px; max-height:10%", classes: "enyo-border-box list-item", ontap: "itemTap", components: [
            {layoutKind: "FittableColumnsLayout", components: [
                {name: "filename", classes: "filename"},
                {name: "filesize", classes: "filesize"}
              ]}
          ]}
      ]},
    {name: "openingSpinner", kind: "onyx.Popup", classes: "popup", centered: true, modal: true, floating: true, autoDismiss: false, components: [
        {content: "Please wait, loading..."},
        {style: "border-radius:5px; padding:15px", components: [
            {kind: "onyx.Spinner"}
          ]}
      ]}
  ],
  create: function()
  {
    this.inherited(arguments);
  },
  rendered: function()
  {
    this.inherited(arguments);
  },
  start: function()
  {

    this.files = [];
    console.log("files scene started");
    if (SETTINGS.downloadmap)
    {
      this.$.mapMenuIcon.setSrc("assets/images/checkbox_checked.png");
    }
    else
    {
      this.$.mapMenuIcon.setSrc("assets/images/checkbox_unchecked.png");
    }
    if (navigator.getDeviceStorage)
    {
      console.log("Device storage is accessible");
      var files = navigator.getDeviceStorage('sdcard');
      var cursor = files.enumerate();
      var THIS = this;
      cursor.onsuccess = function(f)
      {
        console.log("Successfully get file");
        var file = f.target.result;
        if (!f.target.result)
        {
          console.log("No more files. Count=" + THIS.files.length);
          THIS.$.list.setCount(THIS.files.length);
          THIS.$.list.refresh();
          if (THIS.files.length < 1)
            THIS.popup("No any GPX files found<br>You need to have existed tracks in .gpx format for using this program");
          return;
        }
        if (file && file.name.match(/.gpx/im))
        {
          THIS.files.push(file);
          console.log("File found: " + file.name + " => " + file.size + " => " + toHuman(file.size));
        }
        cursor.continue();
      };
      cursor.onerror = function()
      {
        console.warn("No files found: " + this.error);
        THIS.popup("No any GPX files found<br>You need to have existed tracks in .gpx format for using this program");
      };
    }
    else
    {
      console.log("Device storage is not accessible");
      this.popup("Cannot get file list");
      this.files = [];
      for (i = 0; i < 10; i++)
      {
        var f = {};
        f.name = "file" + i;
        f.size = i * 100 + 10;
        this.files.push(f);
      }
      this.$.list.setCount(this.files.length);
      this.$.list.refresh();
    }
  },
  setupItem: function(inSender, inEvent)
  {
    var i = inEvent.index;
    var item = this.files[i];
    this.$.filename.setContent(item.name);
    this.$.filesize.setContent(toHuman(item.size));
  },
  itemTap: function(inSender, inEvent)
  {
    var THIS = this;
    if (navigator.getDeviceStorage)
    {
      this.$.openingSpinner.show();
      var storage = navigator.getDeviceStorage("sdcard");
      var request = storage.get(this.files[inEvent.index].name);
      request.onsuccess = function()
      {
        var name = this.result.name;
        console.log('File "' + name + '" successfully retrieved from the sdcard storage area');
        var reader = new FileReader();
        reader.readAsText(this.result);
        reader.onload = function()
        {
          var filedata = this.result;

          if (parseGPX(filedata))
          {
            THIS.$.openingSpinner.hide();
            if (SETTINGS.downloadmap)
            {
              THIS.$.mapprogress.setProgress(0);
              THIS.$.mapdownloadingpopup.show();
              getMap(TRACK, THIS.updateMapDownloadingProgress.bind(THIS));
            }
            else
              THIS.back();
          }
          else
          {
            THIS.$.openingSpinner.hide();
            THIS.popup("Cannot parse file");
          }
        };
        reader.onerror = function()
        {
          THIS.$.openingSpinner.hide();
          console.log("Cannot read file");
          this.popup("Cannot read file<br>" + this.error.name);
        };
      };
      request.onerror = function()
      {
        console.warn('Unable to get the file: ' + this.error);
        THIS.$.openingSpinner.hide();
        THIS.popup("Unable to open file <br>" + this.error.name);
      };
    }
    else if (!enyo.platform.firefoxos)
    {
      parseGPX(filedata);
      if (SETTINGS.downloadmap)
      {
        this.$.mapprogress.setProgress(0);
        this.$.mapdownloadingpopup.show();
        getMap(TRACK, this.updateMapDownloadingProgress.bind(this));
      }
      else
        this.back();
    }
  },
  updateMapDownloadingProgress: function(value, maximum)
  {
    var a = value / maximum * 100;
    console.log("Progress:" + a);
    this.$.mapprogress.setProgress(a);
    if (value >= maximum)
    {
      this.$.mapdownloadingpopup.hide();
      doneMapDownloading();
      this.back();

    }
  },
  menuSelected: function(inSender, inEvent)
  {
    //Menu items send an onSelect event with a reference to themselves & any directly displayed content
    if (inEvent.originator)
    {
      switch (inEvent.originator.name)
      {
        case "refreshMenu":
          this.start();
          break;
        case "mapMenu":
          SETTINGS.downloadmap = !SETTINGS.downloadmap;
          if (SETTINGS.downloadmap)
          {
            this.$.mapMenuIcon.setSrc("assets/images/checkbox_checked.png");
            this.popup("Warning! Map downloading and processing cat take a lot of time. Please be patient or disable this feature");
          }
          else
          {
            this.$.mapMenuIcon.setSrc("assets/images/checkbox_unchecked.png");
          }
          saveSettings();
          break;
        case "aboutMenu":
          this.popup(ABOUT_TEXT);
          break;
      }
    }
  },
  popup: function(message)
  {
    this.$.popup.setContent(message);
    this.$.popup.show();
  },
  setBack: function(f)
  {
    this.back = f;
  }
});